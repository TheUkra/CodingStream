#!/bin/bash
while true
do
	# Adds the currently playing iTunes track to a text file for an OBS text label.
	#
	SONG=`osascript ./iTunesSong.scpt`
	echo -e $SONG > ./tmp/nowPlaying.txt
	date > ./tmp/dateTime.txt
	sleep 1
done